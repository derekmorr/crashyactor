package org.derekmorr

import akka.actor.{Props, ActorLogging, Actor}
import akka.event.LoggingReceive
import org.derekmorr.CrashyActor._

/**
 * An Actor that crashes after every message.
 */
class CrashyActor extends Actor with ActorLogging {

  override def receive = LoggingReceive {
    case SaySomething(msg) => {
      log.info(s"received message: $msg")
      throw new SuicideException("Goodbye cruel world!")
    }
  }

  // callback called on the new actor.
  override def postRestart(reason: Throwable) = {
    log.info("created a new actor.")
  }
}

object CrashyActor {
  case class SaySomething(message: String)

  // factory method
  def props: Props = Props(new CrashyActor())
}

class SuicideException(msg: String) extends Exception
