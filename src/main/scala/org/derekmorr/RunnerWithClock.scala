package org.derekmorr

import scala.concurrent.duration._
import scala.concurrent.duration.Duration.Zero
import scala.language.postfixOps

import akka.actor.{Props, ActorSystem}
import org.derekmorr.ClockActor.Tick
import org.derekmorr.CrashyActor.SaySomething

/**
 * Driver showing two actors
 */
object RunnerWithClock extends App {

  val sentences = (1 to 9) map { i => s"This is sentence $i." }

  println("starting up actor system")
  val actorSystem = ActorSystem("CrashySystem")

  val crashyActor = actorSystem.actorOf(Props[CrashyActor], "crashyactor")
  val clockActor  = actorSystem.actorOf(Props[ClockActor],  "clockactor")

  import actorSystem.dispatcher
  actorSystem.scheduler.schedule(Zero, 10 milliseconds, clockActor, Tick)

  sentences foreach { sentence => crashyActor ! SaySomething(sentence) }

  Thread.sleep(500)
  println("shutting down actor system")
  actorSystem.shutdown()
}
