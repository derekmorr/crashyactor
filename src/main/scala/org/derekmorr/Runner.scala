package org.derekmorr

import akka.actor.{Props, ActorSystem}
import akka.routing.FromConfig
import org.derekmorr.CrashyActor.SaySomething

/**
 * Driver for CrashyActor
 */
object Runner extends App {

  val sentences = (1 to 9) map { i => s"This is sentence $i." }

  println("starting up actor system")
  val actorSystem = ActorSystem("CrashySystem")

  val crashyActor = actorSystem.actorOf(Props[CrashyActor], "crashyactor")
  //val crashyActor = actorSystem.actorOf(Props[CrashyActor].withRouter(FromConfig()), "crashyactor")

  sentences foreach { sentence => crashyActor ! SaySomething(sentence) }

  Thread.sleep(2000)
  println("shutting down actor system")
  actorSystem.shutdown()
}
