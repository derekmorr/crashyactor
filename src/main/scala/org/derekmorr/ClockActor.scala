package org.derekmorr

import akka.actor.{Actor, ActorLogging}
import org.derekmorr.ClockActor.Tick

class ClockActor extends Actor with ActorLogging {

  override def receive = {
    case Tick => log.info(s"current time: ${System.currentTimeMillis()}")
  }
}

object ClockActor {
  case object Tick
}
