package org.derekmorr

import scala.util.Random

import akka.actor.ActorSystem
import org.derekmorr.CrashyActor.SaySomething
import org.scalatest.{OneInstancePerTest, BeforeAndAfterAll, WordSpecLike, MustMatchers}

/**
 * Simple tests for CrashyActor.
 */
class CrashyActorTest extends MustMatchers with WordSpecLike with BeforeAndAfterAll with OneInstancePerTest {

  import akka.testkit.EventFilter
  import com.typesafe.config.ConfigFactory

  // actor system that sends logs to the test event listener.
  implicit val system = ActorSystem("testsystem",
                                    ConfigFactory.parseString("""akka.loggers = ["akka.testkit.TestEventListener"]"""))

  val testSize = 50
  val messageSize = 10
  val randomStrings = Seq.fill(testSize)(Random.nextString(messageSize))

  "CrashyActor" must {

    val crashyActor = system.actorOf(CrashyActor.props)

    "send messages to the log stream at info level" in {
      EventFilter.info(source = crashyActor.path.toString,
                       occurrences = testSize,
                       pattern = "received message: *") intercept {
        randomStrings foreach { str => crashyActor ! SaySomething(str) }
      }
    }
  }

  /** shutdown the actor system when all tests are done. */
  override protected def afterAll {
    super.afterAll
    system.shutdown
  }

}
