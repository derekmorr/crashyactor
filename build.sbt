name := "crashyactor"

version := "1.0"

scalaVersion := "2.11.6"

val akkaVersion = "2.3.9"

scalacOptions ++= Seq("-feature", "-unchecked", "-deprecation", "-Xcheckinit",
                      "-Xlint", "-Xfatal-warnings", "-g:line",
                      "-Ywarn-dead-code",  "-Ywarn-numeric-widen")

libraryDependencies ++= {
  Seq(
    "com.typesafe.akka" %% "akka-actor" % akkaVersion % Compile withSources() withJavadoc(),
    "com.typesafe.akka" %% "akka-testkit" % akkaVersion  % Test withSources() withJavadoc(),
    "org.scalatest" %% "scalatest" % "2.2.4" % Test withSources() withJavadoc()
  )
}

mainClass in (Compile, run) := Some("org.derekmorr.Runner")
//mainClass in (Compile, run) := Some("org.derekmorr.RunnerWithClock")

