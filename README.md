# Actor basics

This project shows the basics of actor messaging, fault isolation and supervision, and load-balancing.

# Prerequisites

You will need [Java installed](http://java.oracle.com/). The app will download any additional dependencies.

# Branches

This project has three git branches:

 * well-behaved: basic Actor message processing without errors
 * master: Actor message processing with fault-tolerance
 * load-balanced: configuration-based load balancing
 
You switch between them by running `git co <branch>` from the command line. 

# Running the app:

Once on the desired branch, run 

    ./sbt run

# Manually installing dependencies

The project depends on [Scala](http://www.scala-lang.org/) and its build tool, [sbt](http://www.scala-sbt.org/). If the `sbt` script fails to install Scala and SBT, you can manually install them. 

On OS X, assuming you have [homebrew](http://brew.sh/) installed, the easiest way to install these is via:

    brew install scala sbt
    
Alternatively, you can download native OS packages from the links above.